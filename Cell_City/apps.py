from django.apps import AppConfig


class CellCityConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Cell_City'
